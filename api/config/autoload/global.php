<?php

return [
    'db' => [
        'driver' => 'Pdo',
        'dsn' => 'mysql:dbname=inp;host=mysql;charset=utf8',
        'username' =>  getenv('DB_USER'),
        'password' => getenv('DB_PASSWORD')
    ],
];
