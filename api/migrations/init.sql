CREATE TABLE IF NOT EXISTS migrations (
    version text
);

CREATE TABLE IF NOT EXISTS number
(
    user    varchar(250),
    number1 integer,
    number2 integer,
    number3 integer,
    number4 integer,
    guess   integer,
    id      bigint NOT NULL AUTO_INCREMENT,
    CONSTRAINT id PRIMARY KEY (id)
);