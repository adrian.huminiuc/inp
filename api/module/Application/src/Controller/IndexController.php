<?php

namespace Application\Controller;

use Application\Model\Number;
use Application\Model\NumberTable;
use Application\Services\NumberGuesser;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

class IndexController extends AbstractRestfulController
{
    /**
     * @var NumberTable
     */
    private $numberTable;

    public function __construct(NumberTable $numberTable)
    {
        $this->numberTable = $numberTable;
    }

    public function indexAction(): JsonModel
    {
        foreach ($this->numberTable->getAll() as $item) {
            $data[] = $item->toArray();
        }
        
        return new JsonModel($data ?? []);
    }

    
    public function guessAction(): JsonModel
    {
        $numberGuess = new Number();
        $numberGuess->exchangeArray(
            $this->jsonDecode($this->request->getContent())
        );

        try {
            $numberGuess->validate();
        } catch (\Exception $e) {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel(['error' => $e->getMessage()]);
        }
        
        $numberGuess->guess = (new NumberGuesser())->guess($numberGuess);
        $id = $this->numberTable->save($numberGuess);

        $this->getResponse()->setStatusCode(201);
        return new JsonModel(
            [
                'id' => $id,
                'guess' => $numberGuess->guess
            ]
        );
    }
}
