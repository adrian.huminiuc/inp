<?php


namespace Application\Model;

use RuntimeException;
use Zend\Db\TableGateway\TableGatewayInterface;

class NumberTable
{
    private $tableGateway;

    public function __construct(TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function getAll()
    {
        foreach ($this->tableGateway->select() as $row) {
            yield $row;
        }
    }

    public function getAlbum(int $id)
    {
        if (!$row = $this->tableGateway->select(['id' => $id])->current()) {
            throw new RuntimeException(
                sprintf(
                    'Could not find row with identifier %d',
                    $id
                )
            );
        }

        return $row;
    }

    public function save(Number $number)
    {
        $data = [
            'user' => $number->user,
            'guess' => $number->guess,
            'number1' => $number->number1,
            'number2' => $number->number2,
            'number3' => $number->number3,
            'number4' => $number->number4,
        ];

        if ((int)$number->id === 0) {
             $this->tableGateway->insert($data);
             return $this->tableGateway->lastInsertValue;
        }

        $this->tableGateway->update($data, ['id' => (int)$number->id]);
        return (int)$number->id;
    }
}