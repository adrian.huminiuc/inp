<?php


namespace Application\Model;


class Number extends \ArrayObject 
{
    /**
     * @var int|null
     */
    public $id;
    /**
     * @var string
     */
    public $user;

    /**
     * @var int|null
     */
    public $number1;
    /**
     * @var int|null
     */
    public $number2;
    /**
     * @var int|null
     */
    public $number3;
    /**
     * @var int|null
     */
    public $number4;
    /**
     * @var int|null
     */
    public $guess;
    
    public function exchangeArray($data)
    {
        $this->id = $data['id'] ?? null;
        $this->user = $data['user'] ?? null;;
        $this->number1 = $data['number1'] ?? null;
        $this->number2 = $data['number2'] ?? null;
        $this->number3 = $data['number3'] ?? null;
        $this->number4 = $data['number4'] ?? null;
        $this->guess = $data['guess'] ?? null;
    }

    public function validate()
    {
        if (empty($this->user)) {
            throw new \Exception('User cannot be empty');
        }

        if (empty($this->number1)
            || empty($this->number2)
            || empty($this->number3)
            || empty($this->number4)
        ) {
            throw new \Exception('Number fields need to be filled and more than 0');
        }

        $numbers = [$this->number1, $this->number2, $this->number3, $this->number4];
        foreach ($numbers as $number) {
            if ($number > 999) {
                throw new \Exception('Sorry the numbers are too large, try smaller ones!');
            }
        }
    }

    public function toArray()
    {
        return [
            'user' => $this->user,
            'guess' => $this->guess,
            'number1' => $this->number1,
            'number2' => $this->number2,
            'number3' => $this->number3,
            'number4' => $this->number4,
        ];
    }
}