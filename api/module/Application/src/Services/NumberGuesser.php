<?php


namespace Application\Services;


use Application\Model\Number;

class NumberGuesser
{
    public function guess(Number $number)
    {
        $numbers = [
            $number->number1,
            $number->number2,
            $number->number3,
            $number->number4
        ];

        $deviation = $this->deviation($numbers);
        $maxValue = max($numbers);
        $minValue = min($numbers);

        if ($maxValue >= $deviation) {
            return $maxValue + $deviation;
        }

        return $minValue - $deviation;
    }

    private function deviation($arr)
    {
        $num_of_elements = count($arr);
        $variance = 0.0;
        $average = array_sum($arr)/$num_of_elements;

        foreach($arr as $i)
        {
            // sum of squares of differences between  
            // all numbers and means. 
            $variance += pow(($i - $average), 2);
        }

        return floor(sqrt($variance/$num_of_elements));
    }
}