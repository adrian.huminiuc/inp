#### Dependencies 
```
docker
docker-compose
```

#### Download 
```
git clone https://gitlab.com/adrian.huminiuc/inp.git
cd inp
```

#### To run 

1. Make a new .env file, you can copy the  env.local one
```
cp .env.local .env
```
Set HOST_IP= 
To the result of: hostname -I |awk '{print $1}'

The rest of the variables will be used to create the db and initial configs
can set whatever you wish

2. Start docker
```
docker-compose up -d
```

3. Install composer dependencies
```
docker exec -it inp_api composer install
```

4. Setup initial schema
```
docker exec -i inp_mysql mysql -uUSER_FROM_ENV -pUSER_PASSWORD_FROM_ENV inp < api/migrations/init.sql
```

5. Navigate to the http://HOST_IP  should be visible.


