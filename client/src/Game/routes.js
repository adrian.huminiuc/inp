import Guess from './Guess';
import Present from './Present';

export const gameRoutes = [
    {path: '/', name: "game", component: Guess},
    {path: '/present', name: "present", component: Present},
];