import Api from '../api'

export default {
    state: {
        game: {
            id: null,
            user: null,
            guess: null,
            number1: null,
            number2: null,
            number3: null,
            number4: null,
        },
    },
    mutations: {
        setGameResults(state, gameResults) {
            Object.assign(state.game, gameResults);
        },
    },
    actions: {
        async getNextGuess({commit}, input) {
            return Api.getGuess(input).then(
                json => {
                    const payload = {guess: json.guess, id: json.id, ...input};
                    commit('setGameResults', payload);
                },
            );
        },
    },
};