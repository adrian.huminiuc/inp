class Api {
    baseUrl = process.env.VUE_APP_API_URL;
    
    async getGuess(input) {
        const response = await fetch(this.baseUrl + '/numbers/guess', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(input),
        });

        const json = await response.json();

        if (response.status > 300) {
            return Promise.reject({error: json.error});
        }

        return Promise.resolve(json);
    }
}

export default new Api();