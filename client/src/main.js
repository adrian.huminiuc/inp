import Vue from 'vue';
import Vuex from 'vuex';
import App from './App.vue';
import VueRouter from 'vue-router';
import {gameRoutes} from './Game/routes';
import gameStore from './Game/store/store';

Vue.use(Vuex);
Vue.use(VueRouter);
Vue.config.productionTip = false;

const router = new VueRouter({
    routes: gameRoutes,
});

const store = new Vuex.Store(gameStore);

new Vue({
    router,
    store,
    render: h => h(App),
}).$mount('#app');
